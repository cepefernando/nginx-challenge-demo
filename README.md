# Nginx Webserver ArgoCD Demo

### Environment Variables

These Environment Variables are needed for the pipeline when
runnig Terraform commands.

* `AWS_ROLE_ARN` - AWS Role Arn to used by the pipeline to get temporary credentials
* `ECR_REPO ` -  ECR URI where we are pushing the image
* `SSH_PRIVATE_KEY`  -  SSH Key for updating the chart manifest repository

# CI/CD

We are leveraging the use of GitLabCI for doing the CI/CD of the applications, both for building and deploying the Docker image into an EKS cluster using ArgoCD.

![Alt text](./diagram-ci.png?raw=true "Title")